%{ 
Università degli Studi di Verona
Analisi di difetti di tessiture - Elaborazione di segnali e immagini

Anno Accademico 2021-2022

Luca Lovato  - VR443506
Matteo Bauce - VR451800
%}

% Il testing é stato effettuato su 4 cartelle differenti: run1, run2, run3, run4
% Ognuna di queste cartelle contiene 6 immagini per un totale di 24 immagini

clear all
close all
clc

foldRun = 'images/run1/';
images = dir(strcat(foldRun,'*.jpg'));                  % acquisisce tutte le immagini nella cartella run

for i=1:size(images,1)                                  % scorre tutte le immagini

    path = strcat(foldRun, images(i).name);             % concatena il nome della foldRun con il nome dell'immagine
    
    image = rgb2gray(imread(path));                     % trasforma l'immagine in scala di grigi

    [rows,columns] = size(image);                       % assegna il numero di righe e il numero di colonne 
    image = imadjust(image, stretchlim(image), []);     % esegue lo stretching dell'immagine per aumentare il contrasto
    
    % figure; subplot(1,2,1); imshow(image); colorbar; title('Immagine originale')
    
    MIN = 0; MAX = 255; gamma = 2;
    
    sTilde = double(image).^gamma;                      % trasformazione di potenza
    sTildeMIN = double(min(sTilde(:)));
    sTildeMAX = double(max(sTilde(:)));
    
    % trasformazione non lineare
    image = ((sTilde - sTildeMIN)./(sTildeMAX-sTildeMIN))*(MAX-MIN)+MIN;
    image = uint8(image);                               % converte l'immagine in 8 bit              
    
    % subplot(1,2,2); imshow(image); colorbar; title('Immagine modificata') 
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% patterns %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    patterns = cell(1,6);                              % array di cell utilizzato per memorizzare sequenze di matrici
    
    patterns{1} = image(1:14,1:14);                                
    patterns{2} = image(2:15,2:15);                                
    patterns{3} = image(rows-13:rows,columns-13:columns);          
    patterns{4} = image(rows-14:rows-1,columns-14:columns-1);      
    patterns{5} = image(1:14,columns-13:columns);                  
    patterns{6} = image(2:15,columns-13:columns);                  
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % cross correlazione normalizzata tra immagine in scala di grigi e patterns 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    meanTemp = 0;
    
    for j=1:6
        meanTemp = meanTemp + normxcorr2(patterns{j}, image);      % somma i risultati delle cross-correlazioni tra patterns e immagine
    end
    meanResults = meanTemp/6;                                      % esegue la media dei valori in meanTemp
    
    meanResults = meanResults(13:end-13, 13:end-13);               % rimuove "effetto bordo" -> 500x500
    meanResults = abs(meanResults);                                % esegue il modulo della media stimata
    
    % figure; imagesc(meanResults), colorbar; title('cross correlazione normalizzata 2D')
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% maschere %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    grayLevel = graythresh(meanResults);                                      % utilizza il metodo di Otsu per minimizzare la varianza d'interclasse 
   
    threshold = grayLevel/2.1;                                                % calcola la soglia di visibilità del difetto 
    filteredImage = stdfilt(meanResults, ones(11));                           % calcola la deviazione standard della media
   
    % figure; imagesc(filteredImage), colorbar; title 'Immagine filtrata con deviazione standard';

    firstMask = filteredImage < threshold;                                    % filtra il difetto
    
    % figure; subplot(1,2,1); imagesc(firstMask); title('maschera 1');        % prima maschera 
    
    structuringElement = strel('disk', 4);                                    % crea un disco di raggio 4 per evidenziare il difetto
    secondMask = imopen(firstMask, structuringElement); 
    % figure; subplot(1,2,1); imagesc(secondMask); title('maschera 2');

    figure(1); set(gcf, 'Position', get(0, 'Screensize'));                    % posiziona la figura corrente
    
    subplot(2,3,i);
    imagesc(secondMask); title(strcat('maschera', " ", int2str(i))); 
    
    pause(1);                                                                 % pausa
        
    % subplot(1,2,2); imagesc(secondMask); title('maschera 2');               % seconda maschera
    
    image = image(6:end-7, 6:end-7);                                          % adatta l'immagine per il confronto con l'immagine modificata durante il processo
    
    maskImage = image;
    maskImage(secondMask) = 255;                                              % valuta il difetto sulla scala di grigi
    defectImage = cat(3, maskImage, image, image);                            % concatena l'immagine originale e l'immagine con difetto
       
    figure(2); set(gcf, 'Position', get(0, 'Screensize'));                    % posiziona la figura corrente

    subplot(2,3,i);
    imshowpair(image, defectImage, 'montage');                                % crea un montaggio tra immagine originale e immagine con difetto 
    title(strcat('difetto'," ", int2str(i), ", threshold: ", num2str(round(threshold, 3))));
    
    pause(1);

end